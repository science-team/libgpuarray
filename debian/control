Source: libgpuarray
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Ghislain Antony Vaillant <ghisvail@gmail.com>,
 Rebecca N. Palmer <rebecca_palmer@zoho.com>
Section: libs
Priority: optional
Build-Depends: cmake,
               cython3 (>= 0.25),
               debhelper-compat (= 13),
               dpkg-dev (>= 1.17.14),
               dh-python,
               python3-all-dev,
               python3-numpy,
               python3-setuptools,
               sphinx-common,
               python3-mako
Build-Depends-Indep: python3-breathe <!nodoc>,
                     python3-sphinx <!nodoc>,
                     python3-sphinx-rtd-theme <!nodoc>
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/science-team/libgpuarray
Vcs-Git: https://salsa.debian.org/science-team/libgpuarray.git
Homepage: https://github.com/Theano/libgpuarray

Package: libgpuarray3
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
#need the -dev because it dlopen()s the un-numbered names
         ocl-icd-opencl-dev | nvidia-cuda-dev
Recommends: libclblast-dev | libclblas-dev | nvidia-cuda-dev
Description: library to manipulate tensors on the GPU
 libgpuarray provides a ndarray (multi-dimensional array) object which
 is computed on the GPU. It supports int, single and double precision
 floats.
 .
 libgpuarray supports Nvidia's CUDA interface as well as OpenCL. The
 Debian packages have been build against OpenCL. However, the source
 package could be rebuild locally also for CUDA (which is non-free)
 without changes, if that's needed.
 .
 This package provides the shared library.

Package: libgpuarray-dev
Architecture: any
Section: libdevel
Depends: ${misc:Depends},
         libgpuarray3 (= ${binary:Version})
Suggests: libgpuarray-doc <!nodoc>
Description: development files for libgpuarray
 libgpuarray provides a ndarray (multi-dimensional array) object which
 is computed on the GPU. It supports int, single and double precision
 floats.
 .
 libgpuarray supports Nvidia's CUDA interface as well as OpenCL. The
 Debian packages have been build against OpenCL. However, the source
 package could be rebuild locally also for CUDA (which is non-free)
 without changes, if that's needed.
 .
 This package provides the development files.

Package: libgpuarray-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Built-Using: ${sphinxdoc:Built-Using}
Description: documentation for libgpuarray
 libgpuarray provides a ndarray (multi-dimensional array) object which
 is computed on the GPU. It supports int, single and double precision
 floats.
 .
 libgpuarray supports Nvidia's CUDA interface as well as OpenCL. The
 Debian packages have been build against OpenCL. However, the source
 package could be rebuild locally also for CUDA (which is non-free)
 without changes, if that's needed.
 .
 This package provides the documentation.
Build-Profiles: <!nodoc>

Package: python3-pygpu
Architecture: any
Section: python
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends},
         libgpuarray3 (= ${binary:Version})
Suggests: libgpuarray-doc <!nodoc>
Description: language bindings for libgpuarray (Python 3)
 libgpuarray provides a ndarray (multi-dimensional array) object which
 is computed on the GPU. It supports int, single and double precision
 floats.
 .
 libgpuarray supports Nvidia's CUDA interface as well as OpenCL. The
 Debian packages have been build against OpenCL. However, the source
 package could be rebuild locally also for CUDA (which is non-free)
 without changes, if that's needed.
 .
 This package provides the language bindings for Python 3.
